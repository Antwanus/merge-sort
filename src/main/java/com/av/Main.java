package com.av;

import java.security.SecureRandom;
import java.util.Random;
import java.util.logging.Logger;

public class Main {
   public static final Logger LOG = Logger.getLogger(Main.class.getName());
   private static final Random RANDOM = new SecureRandom();

   public static void main(String[] args) {
      int[] numbers = new int[10_000_000];
      for (int i = 0; i < numbers.length; i++)  numbers[i] = RANDOM.nextInt(1000_000_000);

      LOG.info(() -> "Unsorted array:");
      printArray(numbers);

      long start = System.currentTimeMillis();
      mergesort(numbers);
      long stop = System.currentTimeMillis() - start;

      LOG.info(() -> "Sorted array:");
      printArray(numbers);

      LOG.info(() -> "Timelapse= " +  stop + "ms");


   }

   private static void mergesort(int[] array) {
      if (array.length < 2)    return;  // => 1 element array == sorted :)

      int inputLength = array.length;
      int midIndex = inputLength / 2;
      int[] leftHalf = new int[midIndex];
      int[] rightHalf = new int[inputLength - midIndex];
      System.arraycopy(array, 0, leftHalf, 0, midIndex);
      System.arraycopy(array, midIndex, rightHalf, 0, (inputLength - midIndex));

      mergesort(leftHalf);
      mergesort(rightHalf);
      merge(array, leftHalf, rightHalf);
   }

   private static void merge(int[] mergeArray, int[] leftHalf, int[] rightHalf) {
      int leftSize = leftHalf.length;
      int rightSize = rightHalf.length;
      int leftIndex = 0, rightIndex = 0, mergedIndex = 0;

      // compare the two halves & fill mergeArray until one half runs out of elements
      while (leftIndex < leftSize && rightIndex < rightSize) {
         if (leftHalf[leftIndex] <= rightHalf[rightIndex])
            mergeArray[mergedIndex++] = leftHalf[leftIndex++];
         else
            mergeArray[mergedIndex++] = rightHalf[rightIndex++];
      }
      // add remaining (ordered) elements
      while (leftIndex < leftSize)
         mergeArray[mergedIndex++] = leftHalf[leftIndex++];
      while (rightIndex < rightSize)
         mergeArray[mergedIndex++] = rightHalf[rightIndex++];
   }

   private static void printArray(int[] numbers) {
      StringBuilder result = new StringBuilder("[");
      int lastIndex = numbers.length - 1;

      for (int i = 0; i < numbers.length; i++) {
         result.append(numbers[i]);
         if (i != lastIndex)
            result.append(", ");
      }
      result.append("]");

      LOG.info(() -> String.valueOf(result));
   }
}
